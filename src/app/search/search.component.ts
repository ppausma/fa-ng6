import { Component, OnInit, NgModule } from '@angular/core';
import { DataService } from '../data.service';
import { TruncateLessMorePipe } from '../pipes/truncate-less-more.pipe';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  btnText: string = "Search";
  visible = false;
  searchQuery: string;
  plotLength: string;
  movie: any;
  more: boolean;

  //store comma seperated string of languages
  langList;

  genreList;
  writerList;
  actorList;

  constructor(private movieService_: DataService) { }

  ngOnInit() {
  }

  selectChangeHandler(event: any) {
    this.plotLength = event.target.value;
    //console.log(this.plotLength);
  }

  searchMovie() {
    this.movie = this.movieService_.searchMovies(this.searchQuery, this.plotLength)
    .subscribe(data => {
      this.movie = data;
      // this.langList = this.movie.Language;
      this.langList = this.movie.Language.split(',');

      this.genreList = this.movie.Genre.split(',');

      this.writerList = this.movie.Writer.split(',');

      this.actorList = this.movie.Actors.split(',');

    });

    this.visible = true;
  }

  showMore() {
   this.more = !this.more;
  }

}
