import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private url: string = "http://www.omdbapi.com/?apikey=ee5c3346";

  constructor(private http_:HttpClient) { }

  getMovies(title: string) {
    return this.http_.get(`http://www.omdbapi.com/?s=${encodeURI(title)}&apikey=ee5c3346`)
  }

  getMovie(imdbID: string) {
    return this.http_.get(`http://www.omdbapi.com/?i=${imdbID}&apikey=ee5c3346`)
  }

  searchMovies(title, plot) {
    //init params object
    let Params = new HttpParams();

    //assign params
    Params = Params.append('t', title);
    Params = Params.append('plot', plot);

    //returns Observable
    return this.http_.get(this.url, { params: Params})
  }

}
