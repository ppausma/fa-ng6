import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FeaturedComponent } from './featured/featured.component';
import { SearchComponent } from './search/search.component';

import { HttpClientModule } from '@angular/common/http';
import { LoadingSpinnerComponent } from './ui/loading-spinner/loading-spinner.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { TruncateLessMorePipe } from './pipes/truncate-less-more.pipe';

@NgModule({
  declarations: [
    AppComponent,
    FeaturedComponent,
    SearchComponent,
    LoadingSpinnerComponent,
    SidebarComponent,
    TruncateLessMorePipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
