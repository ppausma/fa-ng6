import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-featured',
  templateUrl: './featured.component.html',
  styleUrls: ['./featured.component.scss']
})
export class FeaturedComponent implements OnInit {

  movie01: string = 'tt4881806';
  movie02: string = 'tt3731562';
  movie01$: any;
  movie02$: any;

  movieRdy01: boolean = false;
  movieRdy02: boolean = false;

  showSpinner: boolean = true;


  constructor(private data_:DataService) { }

  ngOnInit() {

    this.data_.getMovie(this.movie01)
      .subscribe(res => {

        this.movieRdy01 = true;
        this.movie01$ = res
        console.log('movie1 is ready');

        this.check();
             
    });

    this.data_.getMovie(this.movie02)
      .subscribe(res => {

        this.movieRdy02 = true;
        this.movie02$ = res
        console.log('movie2 is ready');

        this.check();
         
    });

  }

  check(){
    if(this.movieRdy01 === true && this.movieRdy01 === true){
      this.showSpinner = false;
      console.log('spinner off');
    }
  }

}
